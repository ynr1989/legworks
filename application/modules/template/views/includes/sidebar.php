<div class="sidebar color1">
<!-- Sidebar user panel -->
<div class="user-panel text-center">
<!--<div class="image">
    <?php $image = $this->session->userdata('logo') ?>
    <img src="<?php echo base_url((!empty($image) ? $image : 'assets/img/icons/default.jpg')) ?>"
         class="img-circle" alt="User Image">
</div>
<div class="info">
    <p><?php echo $this->session->userdata('user_name') ?></p>
    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('user_level') ?>
    </a>
</div>-->
</div>


<!-- sidebar menu -->
<ul class="sidebar-menu">

<li class="treeview <?php echo(($this->uri->segment(3) == "home" || $this->uri->segment(3) == "home") ? "active" : null) ?>">
    <a href="<?php echo base_url('Admin_dashboard') ?>"><i class="ti-home"></i>
        <span><?php echo display('dashboard') ?></span>
    </a>
</li>


<?php if ($this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 2) { ?>


    <!-- Invoice menu start -->
    <!-- <li class="treeview <?php if (in_array($this->uri->segment('3'),['new_invoice','manage_invoice','invoice_update_form','invoice_inserted_data','pos_invoice'])) { echo 'active';}?>">
        <a href="#">
            <i class="ti-layout-accordion-list"></i><span><?php echo display('sales') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(3) == 'new_invoice' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cinvoice/new_invoice') ?>"><?php echo display('new_sale') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_invoice' || ($this->uri->segment(3) == 'invoice_inserted_data') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cinvoice/manage_invoice') ?>"><?php echo display('manage_sale') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'pos_invoice') ? 'active' : '') ?>">
                <a href="<?php echo base_url('dashboard/Cinvoice/pos_invoice') ?>"><?php echo display('pos_sale') ?></a>
            </li>
        </ul>
    </li> -->
    <!-- Invoice menu end -->

    <!-- Order menu start -->
    <!-- <li class="treeview <?php echo (($this->uri->segment(2) == "Corder")?"active":'')?>">
        <a href="#">
            <i class="ti-truck"></i><span><?php echo display('order') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(3) == 'new_order' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Corder/new_order') ?>"><?php echo display('new_order') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_order' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Corder/manage_order') ?>"><?php echo display('manage_order') ?></a>
            </li>
        </ul>
    </li> -->
    <!-- Order menu end -->

    <!-- Product menu start -->
    <li class="treeview <?php if ($this->uri->segment(2) == ("Cproduct")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-bag"></i><span><?php echo display('product') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Cproduct' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cproduct') ?>"><?php echo display('add_product') ?></a>
            </li>
           
            <li class="<?php echo(($this->uri->segment(3) == 'manage_product' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cproduct/manage_product') ?>"><?php echo display('manage_product') ?></a>
            </li>
             
        </ul>
    </li>
    <!-- Product menu end -->

    <!-- Customer menu start -->
    <!-- <li class="treeview <?php if ($this->uri->segment(2) == ("Ccustomer")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="fa fa-handshake-o"></i><span><?php echo display('customer') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Ccustomer' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Ccustomer') ?>"><?php echo display('add_customer') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_customer' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Ccustomer/manage_customer') ?>"><?php echo display('manage_customer') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'customer_ledger_report' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Ccustomer/customer_ledger_report') ?>"><?php echo display('customer_ledger') ?></a>
            </li>

        </ul>
    </li> -->
    <!-- Customer menu end -->
  
    <!-- Category menu start -->
    <li class="treeview <?php if ($this->uri->segment(2) == ("Ccategory")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-tag"></i><span><?php echo display('category') ?></span>
            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Ccategory' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Ccategory') ?>"><?php echo display('add_category') ?></a>
            </li>
            
            <li class="<?php echo(($this->uri->segment(3) == 'manage_category' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Ccategory/manage_category') ?>"><?php echo display('manage_category') ?></a>
            </li>
        </ul>
    </li>
    <!-- Category menu end -->

    <!-- Brand menu start -->
    <!-- <li class="treeview <?php if ($this->uri->segment(2) == ("Cbrand")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-apple"></i><span><?php echo display('brand') ?></span>
            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Cbrand' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cbrand') ?>"><?php echo display('add_brand') ?></a></li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_brand' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cbrand/manage_brand') ?>"><?php echo display('manage_brand') ?></a>
            </li>
        </ul>
    </li> -->
    <!-- Brand menu end -->


    <!-- Variant menu start -->
    <li class="treeview <?php if ($this->uri->segment(2) == ("Cvariant")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-wand"></i><span><?php echo display('variant') ?></span>
            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Cvariant' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cvariant') ?>"><?php echo display('add_variant') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_variant' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cvariant/manage_variant') ?>"><?php echo display('manage_variant') ?></a>
            </li>
        </ul>
    </li>
    <!-- Variant menu end -->

    <!-- Unit menu start -->
  <!--   <li class="treeview <?php if ($this->uri->segment(2) == ("Cunit")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-ruler-pencil"></i><span><?php echo display('Unit') ?></span>
            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Cunit' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cunit') ?>"><?php echo display('add_unit') ?></a></li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_unit' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cunit/manage_unit') ?>"><?php echo display('manage_unit') ?></a>
            </li>
        </ul>
    </li> -->
    <!-- Unit menu end -->

    <!-- Gallery menu start -->
    <li class="treeview <?php if ($this->uri->segment(2) == ("Cgallery")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-gallery"></i><span><?php echo display('product_image_gallery') ?></span>
            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo(($this->uri->segment(2) == 'Cgallery' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cgallery') ?>"><?php echo display('add_product_image') ?></a>
            </li>
            <li class="<?php echo(($this->uri->segment(3) == 'manage_image' ? 'active' : '')) ?>">
                <a href="<?php echo base_url('dashboard/Cgallery/manage_image') ?>"><?php echo display('manage_product_image') ?></a>
            </li>
        </ul>
    </li>
    

    <?php if ($this->session->userdata('user_type') == '1') { 
	  ?>

         
<?php if($this->session->userdata('user_id') != 'XHZ8CA5H2651X8L'): ?>
        
       
 
        <!-- Website Settings menu start -->
        <?php $websettlist = array('Cblock','Cweb_setting','Cproduct_review','Csubscriber','Cwishlist','Cweb_footer','Clink_page','Ccoupon','Cabout_us','Cour_location','manage_contact_form','Cshipping_method','manage_slider','manage_add');
         ?>
        <li class="treeview  <?php if(in_array($this->uri->segment(2), $websettlist) || in_array($this->uri->segment(3), $websettlist)){ echo 'active'; } ?>">
            <a href="#">
                <i class="ti-settings"></i><span><?php echo display('web_settings') ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="<?php echo(($this->uri->segment(3) == 'manage_slider' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cweb_setting/manage_slider') ?>"><?php echo display('slider') ?></a>
                </li> 
                <!-- <li class="<?php echo(($this->uri->segment(2) == 'Cblock' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Cblock') ?>"><?php echo display('block') ?> </a>
                </li> -->

                <li class="<?php echo(($this->uri->segment(2) == 'Cproduct_review' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cproduct_review') ?>"><?php echo display('product_review') ?> </a>
                </li>

                <!-- <li class="<?php echo (($this->uri->segment(2) == 'Csubscriber' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Csubscriber') ?>"><?php echo display('subscriber') ?> </a>
                </li> --> 

                <li class="<?php echo (($this->uri->segment(2) == 'Cweb_footer' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Cweb_footer/manage_web_footer') ?>"><?php echo display('web_footer') ?> </a>
                </li>

                <li class="<?php echo (($this->uri->segment(2) == 'Clink_page' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Clink_page/manage_link_page') ?>"><?php echo display('link_page') ?> </a>
                </li> 

                <li class="<?php echo (($this->uri->segment(3) == 'manage_contact_form' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cweb_setting/manage_contact_form') ?>"><?php echo display('contact_form') ?> </a>
                </li>

                <li class="<?php echo (($this->uri->segment(2) == 'Cabout_us' ? 'active' : '')) ?>"><a href="<?php echo base_url('dashboard/Cabout_us/manage_about_us') ?>"><?php echo display('why_choose_us') ?> </a>
                </li>

                <li class="<?php echo (($this->uri->segment(2) == 'Cour_location' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cour_location/manage_our_location') ?>"><?php echo display('our_location') ?> </a>
                </li>

               <!--  <li class="<?php echo (($this->uri->segment(2) == 'Cshipping_method' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cshipping_method/manage_shipping_method') ?>"><?php echo display('shipping_method') ?> </a>
                </li> -->

                <li class="<?php echo (($this->uri->segment(3) == 'setting' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Cweb_setting/setting') ?>"><?php echo display('setting') ?> </a>
                </li>
            </ul>
        </li>
        <!-- Website Settings menu end -->

        <!-- Software Settings menu start -->
        <?php $softsetts = array('Company_setup', 'User','Csoft_setting','Language')  ?>
        <li class="treeview <?php echo  (in_array($this->uri->segment(2), $softsetts)?"active":"");  ?>">
            <a href="#">
                <i class="ti-settings"></i><span><?php echo display('software_settings') ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="<?php echo(($this->uri->segment(3) == 'manage_company' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Company_setup/manage_company') ?>"><?php echo display('manage_company') ?></a>
                </li>
                
               <!--  <li class="<?php echo(($this->uri->segment(3) == 'manage_user' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/User/manage_user') ?>"><?php echo display('manage_users') ?> </a>
                </li> -->
               
                <li class="<?php echo(($this->uri->segment(3) == 'color_setting_frontend' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Csoft_setting/color_setting_frontend') ?>"><?php echo display('color_setting_frontend') ?> </a>
                </li>
                <li class="<?php echo(($this->uri->segment(3) == 'color_setting_backend' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Csoft_setting/color_setting_backend') ?>"><?php echo display('color_setting_backend') ?> </a>
                </li>
                <li class="<?php echo(($this->uri->segment(3) == 'email_configuration' ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Csoft_setting/email_configuration') ?>"><?php echo display('email_configuration') ?> </a>
                </li>
 
                <li class="<?php echo(($this->uri->segment(2) == 'Csoft_setting' && ($this->uri->segment(3) == '') ? 'active' : '')) ?>">
                    <a href="<?php echo base_url('dashboard/Csoft_setting') ?>"><?php echo display('setting') ?> </a>
                </li>

            </ul>
        </li>

       
<?php endif; ?>
    <?php }
    ?>

<?php } ?>

<?php if ($this->session->userdata('user_type') == '4') { ?>

    <!-- Invoice menu start -->
    <!-- <li class="treeview <?php if ($this->uri->segment(3) == ("new_invoice") || $this->uri->segment(3) == ("manage_invoice") || $this->uri->segment(3) == ("invoice_update_form")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="ti-layout-accordion-list"></i><span><?php echo display('sales') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo (($this->uri->segment(3) == 'new_invoice' ? 'active':''))?>">
                <a href="<?php echo base_url('dashboard/Store_invoice/new_invoice') ?>"><?php echo display('new_sale') ?></a>
            </li>
            <li class="<?php echo (($this->uri->segment(3) == 'manage_invoice' ? 'active':''))?>">
                <a href="<?php echo base_url('dashboard/Store_invoice/manage_invoice') ?>"><?php echo display('manage_sale') ?></a>
            </li>
        </ul>
    </li> -->
    <!-- Invoice menu end -->

    <!-- POS invoice menu start -->
    <li class="treeview <?php if ($this->uri->segment(3) == ("pos_invoice")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="<?php echo base_url('dashboard/Store_invoice/pos_invoice') ?>" target="_blank">
            <i class="ti-layout-tab-window"></i><span><?php echo display('pos_sale') ?></span>
            
        </a>
    </li>
    <!-- POS invoice menu end -->

    <!-- Customer menu start -->
    <li class="treeview <?php if ($this->uri->segment(2) == ("Ccustomer")) {
        echo "active";
    } else {
        echo " ";
    } ?>">
        <a href="#">
            <i class="fa fa-handshake-o"></i><span><?php echo display('customer') ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo (($this->uri->segment(2) == 'Ccustomer' && ($this->uri->segment(3) == '')  ? 'active':''))?>"><a href="<?php echo base_url('dashboard/Ccustomer') ?>"><?php echo display('add_customer') ?></a></li>
            <li class="<?php echo (($this->uri->segment(3) == 'manage_customer' ? 'active':''))?>">
                <a href="<?php echo base_url('dashboard/Ccustomer/manage_customer') ?>"><?php echo display('manage_customer') ?></a>
            </li>
        </ul>
    </li>
    <!-- Customer menu end -->

   
<?php } ?>

<li class="treeview <?php echo(($this->uri->segment(3) == "manage_requests" || $this->uri->segment(3) == "manage_requests") ? "active" : null) ?>">
    <a href="<?php echo base_url('dashboard/Corder/manage_requests') ?>"><i class="ti-home"></i>
        <span>Manage Requests</span>
    </a>
</li>


</ul>
</div> <!-- /.sidebar -->