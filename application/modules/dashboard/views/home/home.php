<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Admin Home Start -->
<div class="content-wrapper color3">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-world"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('dashboard') ?></h1>
            <small><?php echo display('home') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li class="active"><?php echo display('dashboard') ?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
    <?php
   
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>
        <!-- First Counter -->
        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <a href="<?php echo base_url('dashboard/Ccustomer/manage_customer') ?>">
                    <div class="small-box dashbox dashbox-blue bg-white">
                        <div class="inner">
                            <h3><span class="count-number"><?php echo html_escape($total_customer); ?></span></h3>
                            <p>Total Categories</p>
                        </div>
                        <div class="icon">
                            <img src="<?php echo base_url('my-assets/image/dashboard/user.png') ?>" class="img img-responsive" >
                        </div> 
                    </div>
                </a>
            </div>
            
            <?php if ($this->session->userdata('isAdmin')) { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <a href="<?php echo base_url('dashboard/Cproduct/manage_product') ?>">
                    <div class="small-box dashbox dashbox-info  bg-white">
                        <div class="inner">
                            <h3><span class="count-number"><?php echo html_escape($total_product); ?></span></h3>
                            <p><?php echo display('total_product') ?></p>
                        </div>
                        <div class="icon">
                            <img src="<?php echo base_url('my-assets/image/dashboard/products.png') ?>" class="img img-responsive" >
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <a href="<?php //echo base_url('dashboard') ?>">
                    <div class="small-box dashbox dashbox-red bg-white">
                        <div class="inner">
                            <h3><span class="count-number"><?php echo html_escape($total_suppliers); ?></span></h3>
                            <p>Total Enqueries</p>
                        </div>
                        <div class="icon">
                            <img src="<?php echo base_url('my-assets/image/dashboard/supplier.png') ?>" class="img img-responsive" >
                        </div>
                    </div>
                    </a>
                </div>
                <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <a href="<?php echo base_url('dashboard/Cinvoice/manage_invoice') ?>">
                    <div class="small-box dashbox dashbox-green bg-white">
                        <div class="inner">
                            <h3><span class="count-number"><?php echo html_escape($total_sales); ?></span></h3>
                            <p><?php echo display('total_invoice') ?></p>
                        </div>
                        <div class="icon">
                            <img src="<?php echo base_url('my-assets/image/dashboard/invoice.png') ?>" class="img img-responsive" >
                        </div>
                    </div>
                    </a>
                </div> -->
            <?php } if ($this->session->userdata('user_type') == '4') { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <a href="<?php echo base_url('dashboard/Cinvoice/manage_invoice') ?>">
                    <div class="small-box dashbox dashbox-green bg-white">
                        <div class="inner">
                            <h3><span class="count-number"><?php echo html_escape($total_store_invoice); ?></span></h3>
                            <p><?php echo display('total_invoice') ?></p>
                        </div>
                        <div class="icon">
                            <img src="<?php echo base_url('my-assets/image/dashboard/invoice.png') ?>" class="img img-responsive" >
                        </div>
                    </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <hr>
          
    </section> 
</div> 
<!-- Admin Home end -->

<!-- ChartJs JavaScript -->
<script src="<?php echo base_url() ?>assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>
<?php $this->load->view('../../../dashboard/assets/js/home.php');?>
