<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage order Start -->
<div class="content-wrapper">
<section class="content-header">
<div class="header-icon">
<i class="pe-7s-note2"></i>
</div>
<div class="header-title">
<h1>Manage Requests</h1>
<small>Manage Requests</small>
<ol class="breadcrumb">
<li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
<li class="active">Manage Requests</li>
</ol>
</div>
</section>

<section class="content">
<!-- Alert Message -->
<?php
$message = $this->session->userdata('message');
if (isset($message)) {
?>
<div class="alert alert-info alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $message ?>                    
</div>
<?php 
$this->session->unset_userdata('message');
}
$error_message = $this->session->userdata('error_message');
if (isset($error_message)) {
?>
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $error_message ?>                    
</div>
<?php 
$this->session->unset_userdata('error_message');
}
?>
 


<!-- Manage order report -->
<div class="row">
<div class="col-sm-12">
<div class="panel panel-bd lobidrag">
 
<div class="panel-body">
	<div class="table-responsive">
		<table id="dataTableExample2" class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th><?php echo display('sl') ?></th> 
					<th>Product Name</th>
					<th><?php echo display('customer_name') ?></th>
					<th>Customer Mobile</th>
					<th>Customer Email</th>
					<th>Message</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if ($orders_list) {
					foreach ($orders_list as $order) {
						?>
						<tr>
							<td><?php echo $order['sl']?></td>
							<td><a href="<?php //echo base_url() . 'dashboard/Cproduct/product_details/'.$order['product_id']; ?>"><?php echo $order['product_name']?></a></td>
							<td><?php echo $order['first_name']?> <?php echo $order['last_name']?></td>
							<td><?php echo $order['email']?></td>
							<td><?php echo $order['mobile']?></td>
							<td><?php echo $order['message']?></td>
							<td><?php echo $order['created_at']?></td>

					</tr>
					<?php
				}
			}
			?>
		</tbody>
	</table>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<!-- Manage order End -->